#include "message.h"

MessageChannel::MessageChannel(std::string _channelName, int _msgCount, int _msgSize, bool _isBlocking)
{
    channelName = ('/' + _channelName).c_str();
    queueAttributes.mq_maxmsg = _msgCount;
    queueAttributes.mq_msgsize = _msgSize;

    long flags = 0;
    if (_isBlocking)
        flags = O_CREAT | O_RDWR;
    else
        flags = O_CREAT | O_RDWR | O_NONBLOCK;
    
    queue = mq_open(channelName, flags, 0644, &queueAttributes);

}

void MessageChannel::CloseChannel()
{
    mq_close(queue);
    mq_unlink(channelName);
}

void MessageChannel::SendMessage(std::string _message)
{
    mq_send(queue, _message.c_str(), _message.length(), 0);
}

std::string MessageChannel::GetMessage()
{
    char message[queueAttributes.mq_msgsize] = { 0 };
    int status = mq_receive(queue, message, queueAttributes.mq_msgsize, 0);
    return message;
}