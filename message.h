#pragma once

#include <mqueue.h>
#include <string>

class MessageChannel
{
public: // member variables


private:
    const char *channelName;
    struct mq_attr queueAttributes;
    mqd_t queue;

public: // methods
    MessageChannel(std::string _channelName, int _msgCount, int _msgSize, bool _isBlocking = true);
    
    void CloseChannel();
    void SendMessage(std::string _message);
    std::string GetMessage();

private:

};